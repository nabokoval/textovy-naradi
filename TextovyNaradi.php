<?php

namespace Nabokova\Naradi;

class TextovyNaradi
{
    private $text;
    private $hasTextCleared;
    private $cleartext;
    private $min_length;
    private $max_freq;
    private $frequencyTable = [];

    public function __construct($text, $min_length = null, $max_freq = null) {
        $this->cleartext = null;
        $this->hasTextCleared = false;

        $this->text = $text;
        $this->min_length = ($min_length === null || $min_length < 0) ? null : $min_length;
        $this->max_freq = ($max_freq === null || $max_freq < 0 || $max_freq > 100.0) ? null : $max_freq;
    }

    public function search($text) {
        return false !== mb_strpos($this->getPreparedText(), $this->removeUsualWords($this->clearText($text), $this->getFrequencyTable()));
    }

    private function getPreparedText() {
        if (!$this->hasTextCleared) {
            $text = $this->clearText($this->text);
            $this->prepareFrequencyWordsTable($text);
            $this->cleartext = $this->removeUsualWords($text, $this->getFrequencyTable());
            $this->hasTextCleared = true;
        }

        return $this->cleartext;
    }

    private function clearText($text) {
        return mb_ereg_replace('\s+', ' ', mb_ereg_replace('[^a-z0-9]', ' ', transliterator_transliterate('ASCII/TRANSLIT', mb_convert_case(mb_ereg_replace('[\.,:;\-\r\n\t\(\)\"\']', ' ', $text), 1))));
    }

    private function removeUsualWords(string $text, array $freqTable) {
        $removeByLength = $this->min_length !== null;
        $removeByFrequency = $this->max_freq !== null;
        $minLength = $this->min_length;
        $maxFrequency = $this->max_freq;

        if ($removeByLength || $removeByFrequency) {
            $wordsToRemove = array_filter(
                $freqTable['freq'],
                function($value, $key) use ($removeByLength, $removeByFrequency, $minLength, $maxFrequency) {
                    return ($removeByLength && $value > $maxFrequency)
                        || ($removeByFrequency && mb_strlen($key) < $minLength);
                }, 1);

            foreach (array_keys($wordsToRemove) as $word) {
                $text = mb_ereg_replace("\s$word\s", ' ', $text);
            }
        }

        return $text;
    }

    private function prepareFrequencyWordsTable($text) {
        $words = mb_split("\s+", $text);
        $uniqueWords = array_unique($words);

        $frequencyTable = [
            'total' => count($words),
            'words' => array_fill_keys(array_values($uniqueWords), 0),
            'freq' => null,
        ];

        foreach ($words as $word) {
            $frequencyTable['words'][$word]++;
        }
        asort($frequencyTable['words'], SORT_NUMERIC);
        $maxWordCount = end($frequencyTable['words']);

        $frequencyTable['freq'] = array_fill_keys(array_keys($frequencyTable['words']), 0.0);
        foreach ($frequencyTable['words'] as $word => $occurrences) {
            $frequencyTable['freq'][$word] = round($occurrences * 100.0 / $maxWordCount, 5);
        }

        $this->frequencyTable = $frequencyTable;
    }

    private function getFrequencyTable() { return $this->frequencyTable; }
}

